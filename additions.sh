# Nothing in this file should be substantial enough to be copyrightable, but
# in case of doubt:
#
# To the extent possible under law, the author(s) have dedicated all copyright 
# and related and neighboring rights to this software to the public domain 
# worldwide. This software is distributed without any warranty.
# 
# You should have received a copy of the CC0 Public Domain Dedication along with
# this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>


# Issue new prompt on each command (adds every command to history immediately)
export PROMPT_COMMAND='history -a'


# Useful for launching graphical applications without spitting text everywhere.
launch() { $@ > /dev/null 2> /dev/null & disown; }


# Render lightweight markup to html and view in console browser.
view-markup() { pandoc $1 | w3m -T text/html; }


# Change the terminal title in xterm-like emulators
set_title() { ORIG=$PS1; TITLE="\e]2;$*\a"; PS1=${ORIG}${TITLE}; }


# Keep a diary/journal with a separate file for each day. Calling this function
# will open today's diary entry in your default text editor. Replace ~/Diary
# with wherever you want to store the files.
diary() { ${VISUAL:-${EDITOR:-vi}} ~/Diary/$(date +%Y-%m-%d).txt; }


# Add text to the beginning/end of a file name
#
# usage:  prepend <text-to-prepend> <file-name>
#         append <text-to-append> <file-name>
prepend() { mv "$2" "$1""$2"; }
append()  { mv "$2" "$2""$1"; }


# Use non-breaking spaces with French quoatation marks.
alias guillemets-propres="sed -e 's/« /« /g' -e 's/ »/ »/g'"


# Color codes
COL='\033['    #Prefix all of the following with this
RED='1;31m'
CYAN='1;36m'
GREEN='1;32m'
PURPLE='1;35m'
NC='0m'


# A nice greeting to call when you open the terminal
#
# dependencies: fortune, color codes (above)
welcome() {
	clear
	printf $COL$GREEN
	printf $(whoami)@$HOSTNAME
	printf "$COL$RED "
	printf "$(date '+%A %d %B')"
	printf "$COL$PURPLE "
	printf "$(date '+%R')"
	printf "$COL$NC\n"
	acpi
	fortune -s -n 100
}
welcome


# Perform calculations more easily. You can use 'x' for multiplication instead
# of '*' if you don't weant to have to use quotes (otherwise bash will expand it
# as a glob).
#
# dependencies: bc
#
# ex)  user@computer:~$ calcu 2 x 5
#      10
#      user@computer:~$ calcu '(512 + (19 * 5)) / 2'
#      303.5
#      user@computer:~$
#
# Thank you to NeronLeVelu on stackoverflow for the regex to remove trailing
# zeroes: https://stackoverflow.com/a/30048933
function calcu() { 
	echo "scale=20;$@" | sed -e s/x/*/g | bc | sed '/\./ s/\.\{0,1\}0\{1,\}$//'
}


# Chat with someone logged into the same system. Both partners should run:
#
#     $ tmux-chat <file>
#
# The file must exist, and both users should have write permission to it. It
# will log the conversation.
#
# Combined with SSH, you've got yourself a Slack replacement!
tmux-chat() {
	unset TMUX
	touch "$1"
	tmux new-session "tail -F $1" \; split-window -v 'while read line; do echo "[$(date +%H:%M)] $(whoami): $line" >> '"$1"' && clear; done' \; attach
}


# Print intro to wikipedia article with the specified title
#
# dependencies: jq, curl
#
# ex)  $ wiki "Pokemon Go"
#      Pokémon Go (stylized as Pokémon GO) is an augmented reality (AR)…
wiki() {
	title="$(echo "$1" | sed 's/ /%20/g')"
	url='https://en.wikipedia.org/w/api.php'
	url+='?format=json&action=query&prop=extracts&exintro&explaintext'
	url+='&redirects=1&titles='
	printf "$(curl -sS $url$title | jq '.query.pages' | jq '.[]' \
	| jq '.extract' | sed 's/\\"/"/g')" | awk 1 ORS='\n\n' | tail -c +2 \
	| head -c -3 && printf "\n"
}


# Functions for blocking and unblocking domains through /etc/hosts
hostfile=/etc/hosts
block() {
	if grep -q $1 $hostfile; then
		sudo sed -i -e "s/\t;$1;/\t$1/g" $hostfile
		sudo sed -i -e "s/\t;www.$1;/\twww.$1/g" $hostfile
	else
		sudo sed -i "1s/^/127.0.0.1\t$1\n/" $hostfile
		sudo sed -i "1s/^/127.0.0.1\twww.$1\n/" $hostfile
	fi
}
block-after() {
	sleep $2
	notify-send "Time's up. Blocking $1"
	block $1
}
unblock-forever() {
	sudo sed -i "s/\t$1/\t;$1;/g" $hostfile
	sudo sed -i "s/\twww.$1/\t;www.$1;/g" $hostfile
}
unblock() {
	time=$2
	if [ $# -lt 2 ]; then
		time=300
	fi
	unblock-forever $1
	block-after $1 $time & disown
}

